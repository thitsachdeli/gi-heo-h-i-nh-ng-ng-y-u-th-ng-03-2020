# Giá heo hơi những ngày đầu tháng 03-2020

Trong khi giá heo hơi tại thị trường miền Nam vẫn yên ắng thì giá heo hơi ở miền Bắc trong những ngày đầu tháng 3 đột ngột tăng giá.
Giá heo hơi là giá heo xuất chuồng mà chủ hộ/trang trại chăn nuôi bán cho các thương lái/công ty chế biến thịt heo.